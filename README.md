# Go Cardless Mvp Application

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.1. and GoCardless API https://gocardless.com/

## Development server
Important note: when running app for the first time, please install dependencies by running `npm install` in the root folder.

To run development local server run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build production version
Run `npm run build:prod` to build the project. The build artifacts will be stored in the `dist/` directory.

## Build production version on http-server
Run `npm run build:prod` to build the project. Go to root directory and run `http-server dist`. Navigate to `http://127.0.0.1:8080/go-cardless-mvp/`

## Application flow
The order of pages:

- /billing-request
- /collect-customer
- /collect-bank-account
- /confirm-data
- /create-subscription
