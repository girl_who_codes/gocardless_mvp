export class Utils {
  public static getEmailRegex(): string {
    return '^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$';
  }

  public static getPhoneRegex(): string {
    return '^(?:[+\\d].*\\d|\\d)$';
  }
}
