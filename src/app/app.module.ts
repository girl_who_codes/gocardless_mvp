import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CollectCustomerFormComponent } from './components/collect-customer-form/collect-customer-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CustomerDataService } from './services/customer-data/customer-data.service';
import { BillingRequestFormComponent } from './components/billing-request-form/billing-request-form.component';
import { BankAccountFormComponent } from './components/bank-account-form/bank-account-form.component';
import { PaymentService } from './services/payment/payment.service';
import { CustomerConfirmationComponent } from './components/customer-confirmation/customer-confirmation.component';
import { HeaderComponent } from './components/ui-components/header/header.component';
import { FooterComponent } from './components/ui-components/footer/footer.component';
import { SubscriptionComponent } from './components/subscription/subscription.component';

@NgModule({
  declarations: [
    AppComponent,
    CollectCustomerFormComponent,
    BillingRequestFormComponent,
    BankAccountFormComponent,
    CustomerConfirmationComponent,
    HeaderComponent,
    FooterComponent,
    SubscriptionComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule, HttpClientModule],
  providers: [CustomerDataService, PaymentService],
  bootstrap: [AppComponent],
})
export class AppModule {}
