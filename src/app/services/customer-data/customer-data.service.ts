import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../authentication/authentication.service';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

export interface Customer {
  data: {
    customer: {
      given_name?: string;
      family_name?: string;
      company_name?: string;
      email: string;
      phone_number: string;
    };
    customer_billing_detail: {
      postal_code: string;
      country_code: string;
      city: string;
      address_line1: string;
    };
  };
}

export interface CustomerBankAccount {
  data: {
    account_holder_name: string;
    currency: string;
    country_code: string;
    account_number: string;
    branch_code: string;
  };
}

export interface CustomerIbanAccount {
  data: {
    iban: string;
    account_holder_name: string;
    currency: string;
  };
}

@Injectable({
  providedIn: 'root',
})
export class CustomerDataService {
  private baseUrl = `https://api-sandbox.gocardless.com`;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'GoCardless-Version': '2015-07-06',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
      'Access-Control-Allow-Credentials': 'true',
      'Access-Control-Allow-Method': 'GET, POST',
      Authorization: `Bearer ${this.auth.getAccessToken()}`,
    }),
  };

  constructor(private http: HttpClient, private auth: AuthenticationService, private cookieService: CookieService) {}

  private getBillingId(): string {
    return this.cookieService.get('billingId');
  }

  public collectCustomer(body: Customer): Observable<any> {
    const id = this.getBillingId();
    const url = `${this.baseUrl}/billing_requests/${id}/actions/collect_customer_details`;
    return this.http
      .post(url, body, {
        headers: this.httpOptions.headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(map((response: HttpResponse<any>) => response.body));
  }

  public collectBankAccount(body: CustomerBankAccount | CustomerIbanAccount): Observable<any> {
    const id = this.getBillingId();
    const url = `${this.baseUrl}/billing_requests/${id}/actions/collect_bank_account`;
    return this.http
      .post(url, body, {
        headers: this.httpOptions.headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(map((response: HttpResponse<any>) => response.body));
  }

  public confirmCustomerData() {
    const id = this.getBillingId();
    const url = `${this.baseUrl}/billing_requests/${id}/actions/confirm_payer_details`;
    return this.http
      .post(url, '', {
        headers: this.httpOptions.headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(map((response: HttpResponse<any>) => response.body));
  }

  public fulfilBillingRequest() {
    const id = this.getBillingId();
    const url = `${this.baseUrl}/billing_requests/${id}/actions/fulfil`;
    return this.http
      .post(url, '', {
        headers: this.httpOptions.headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(map((response: HttpResponse<any>) => response.body));
  }
}
