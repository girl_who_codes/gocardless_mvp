import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private token = 'sandbox_NHUzm3MGwGw-f42wVl0V39fz63X2JfPSLY7n5qfR';

  constructor() {}

  public getAccessToken() {
    return this.token;
  }
}
