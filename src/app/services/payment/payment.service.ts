import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { AuthenticationService } from '../authentication/authentication.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

export interface Mandate {
  billing_requests: {
    mandate_request: {
      currency: string;
      scheme: string;
    };
  };
}

export interface Subscription {
  subscriptions: {
    amount: string;
    currency: string;
    interval_unit: string;
    links: {
      mandate: string;
    };
  };
}

@Injectable({
  providedIn: 'root',
})
export class PaymentService {
  private baseUrl = `https://api-sandbox.gocardless.com`;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'GoCardless-Version': '2015-07-06',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
      'Access-Control-Allow-Credentials': 'true',
      Authorization: `Bearer ${this.auth.getAccessToken()}`,
    }),
  };

  constructor(private http: HttpClient, private auth: AuthenticationService, private cookieService: CookieService) {}

  public createBillingRequest(formData: Mandate): Observable<any> {
    const url = `${this.baseUrl}/billing_requests`;
    return this.http
      .post(url, formData, {
        headers: this.httpOptions.headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(map((response: HttpResponse<any>) => response.body));
  }

  public createSubscription(formData: Subscription): Observable<any> {
    const url = `${this.baseUrl}/subscriptions`;
    return this.http
      .post(url, formData, {
        headers: this.httpOptions.headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(map((response: HttpResponse<any>) => response.body));
  }

  public getMandate(): Observable<any> {
    const url = `${this.baseUrl}/mandates`;
    return this.http
      .get(url, {
        headers: this.httpOptions.headers,
        observe: 'response',
        responseType: 'json',
      })
      .pipe(map((response: HttpResponse<any>) => response.body));
  }
}
