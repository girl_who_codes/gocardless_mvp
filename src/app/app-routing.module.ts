import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CollectCustomerFormComponent } from './components/collect-customer-form/collect-customer-form.component';
import { BillingRequestFormComponent } from './components/billing-request-form/billing-request-form.component';
import { BankAccountFormComponent } from './components/bank-account-form/bank-account-form.component';
import { CustomerConfirmationComponent } from './components/customer-confirmation/customer-confirmation.component';
import { SubscriptionComponent } from './components/subscription/subscription.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'billing-request',
    pathMatch: 'full',
  },
  {
    path: 'billing-request',
    component: BillingRequestFormComponent,
  },
  {
    path: 'collect-customer',
    component: CollectCustomerFormComponent,
  },
  {
    path: 'collect-bank-account',
    component: BankAccountFormComponent,
  },
  {
    path: 'confirm-data',
    component: CustomerConfirmationComponent,
  },
  {
    path: 'create-subscription',
    component: SubscriptionComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
