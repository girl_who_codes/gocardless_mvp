import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  CustomerBankAccount,
  CustomerDataService,
  CustomerIbanAccount,
} from '../../services/customer-data/customer-data.service';

@Component({
  selector: 'app-bank-account-form',
  templateUrl: './bank-account-form.component.html',
  styleUrls: ['./bank-account-form.component.scss'],
})
export class BankAccountFormComponent {
  public showIban = false;
  // @ts-ignore
  private formData: CustomerBankAccount | CustomerIbanAccount;

  public customerBankAccountForm: FormGroup = this.fb.group(
    {
      account_holder_name: ['', [Validators.required]],
      currency: ['', [Validators.required]],
      account_number: ['', [Validators.required]],
      branch_code: ['', [Validators.required]],
      country_code: ['', [Validators.required]],
    },
    { updateOn: 'blur' },
  );

  public customerIbanForm: FormGroup = this.fb.group(
    {
      account_holder_name_iban: ['', [Validators.required]],
      iban: ['', [Validators.required]],
      currency_iban: ['', [Validators.required]],
    },
    { updateOn: 'blur' },
  );

  public isFormSubmitted = false;
  public errorFieldRequired = 'This field is required';
  public showSuccessMessage = false;
  public showErrorMessage = false;

  constructor(private fb: FormBuilder, private customerService: CustomerDataService, private router: Router) {}

  get account_holder_name(): AbstractControl {
    return this.customerBankAccountForm.get('account_holder_name') as FormControl;
  }

  get account_holder_name_iban(): AbstractControl {
    return this.customerIbanForm.get('account_holder_name_iban') as FormControl;
  }

  get iban(): AbstractControl {
    return this.customerIbanForm.get('iban') as FormControl;
  }

  get account_number(): AbstractControl {
    return this.customerBankAccountForm.get('account_number') as FormControl;
  }

  get branch_code(): AbstractControl {
    return this.customerBankAccountForm.get('branch_code') as FormControl;
  }

  get country_code(): AbstractControl {
    return this.customerBankAccountForm.get('country_code') as FormControl;
  }

  get currency(): AbstractControl {
    return this.customerBankAccountForm.get('currency') as FormControl;
  }

  get currency_iban(): AbstractControl {
    return this.customerIbanForm.get('currency_iban') as FormControl;
  }

  public checkFormFieldValidity(formField: AbstractControl): boolean {
    return formField.invalid && !formField.value;
  }

  public toggleIbanVisibility() {
    this.showIban = !this.showIban;
  }

  public onSubmit() {
    this.isFormSubmitted = true;

    if (this.customerIbanForm.valid || this.customerBankAccountForm.valid) {
      if (this.showIban) {
        this.formData = {
          data: {
            account_holder_name: this.account_holder_name_iban.value,
            currency: this.currency_iban.value,
            iban: this.iban.value,
          },
        };
      } else {
        this.formData = {
          data: {
            account_holder_name: this.account_holder_name.value,
            currency: this.currency.value,
            country_code: this.country_code.value,
            account_number: this.account_number.value,
            branch_code: this.branch_code.value,
          },
        };
      }

      this.customerService.collectBankAccount(this.formData).subscribe(
        () => (this.showSuccessMessage = true),
        (error) => {
          const errors = error.error.error.errors;
          for (let i = 0; errors.length > i; i++) {
            this.handleCustomErrors(error.error.error.errors[i]);
          }
        },
      );
    }
  }

  public onSuccess() {
    this.router.navigate(['confirm-data']);
  }

  public onError() {
    location.reload();
  }

  private handleCustomErrors(error: { field: string; message: string }) {
    if (error.field) {
      if (this.showIban) {
        if (error.field === 'currency') {
          this.customerIbanForm.get('currency_iban')?.setErrors({ custom: error.message });
        } else if (error.field === 'account_holder_name') {
          this.customerIbanForm.get('account_holder_name_iban')?.setErrors({ custom: error.message });
        } else {
          this.customerIbanForm.get([error['field']])?.setErrors({ custom: error.message });
        }
      } else {
        this.customerBankAccountForm.get([error['field']])?.setErrors({ custom: error.message });
      }
    } else {
      this.showErrorMessage = true;
    }
  }
}
