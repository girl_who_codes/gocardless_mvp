import { Component, OnInit } from '@angular/core';
import { CustomerDataService } from '../../services/customer-data/customer-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-confirmation',
  templateUrl: './customer-confirmation.component.html',
  styleUrls: ['./customer-confirmation.component.scss'],
})
export class CustomerConfirmationComponent implements OnInit {
  public customer = {};
  public fullName = '';
  public address = '';
  public email = '';
  public phone = '';

  public showFulfilBox = false;
  public showError = false;
  public showFulfilError = false;
  public showFulfilSuccess = false;

  constructor(private customerService: CustomerDataService, private router: Router) {}

  ngOnInit() {
    this.formatData();
  }

  public formatData() {
    const customerStorageData = localStorage.getItem('customerData');
    if (customerStorageData) {
      const customerParsed = JSON.parse(customerStorageData).data;
      const customer = customerParsed.customer;
      const billingData = customerParsed.customer_billing_detail;
      this.fullName = `${customer.given_name} ${customer.family_name}`;
      this.address = `${billingData.address_line1}, ${billingData.city}, ${billingData.postal_code}, ${billingData.country_code}`;
      this.phone = `${customer.phone_number}`;
      this.email = `${customer.email}`;
    }
  }

  public fulfilRequest() {
    this.customerService.fulfilBillingRequest().subscribe(
      () => (this.showFulfilSuccess = true),
      () => (this.showFulfilError = true),
    );
  }

  public confirm() {
    this.customerService.confirmCustomerData().subscribe(
      () => (this.showFulfilBox = true),
      () => (this.showError = true),
    );
  }

  public onError() {
    this.router.navigate(['']);
  }

  public goToSubscription() {
    this.router.navigate(['create-subscription']);
  }
}
