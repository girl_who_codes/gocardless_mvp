import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectCustomerFormComponent } from './collect-customer-form.component';

describe('CollectCustomerFormComponent', () => {
  let component: CollectCustomerFormComponent;
  let fixture: ComponentFixture<CollectCustomerFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CollectCustomerFormComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectCustomerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
