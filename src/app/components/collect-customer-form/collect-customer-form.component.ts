import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Utils } from '../../utils/utils';
import { CustomerDataService, Customer } from '../../services/customer-data/customer-data.service';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-collect-customer-form',
  templateUrl: './collect-customer-form.component.html',
  styleUrls: ['./collect-customer-form.component.scss'],
})
export class CollectCustomerFormComponent {
  public hasCompanyName = false;

  public collectCustomerForm: FormGroup = this.fb.group(
    {
      given_name: [''],
      family_name: [''],
      company_name: [''],
      postal_code: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(Utils.getEmailRegex())]],
      phone_number: ['', [Validators.required, Validators.pattern(Utils.getPhoneRegex())]],
      country_code: ['', [Validators.required]],
      city: ['', [Validators.required]],
      address_line1: ['', [Validators.required]],
    },
    { updateOn: 'blur' },
  );

  public isFormSubmitted = false;
  public errorFieldRequired = 'This field is required';
  public errorWrongEmailFormat = 'Wrong e-mail format';
  public errorWrongPhoneNumberFormat = 'Wrong format. Only numbers and "+" is allowed';
  public showSuccessMessage = false;
  public showErrorMessage = false;

  constructor(private fb: FormBuilder, private customerService: CustomerDataService, private router: Router) {}

  get given_name(): AbstractControl {
    return this.collectCustomerForm.get('given_name') as FormControl;
  }

  get family_name(): AbstractControl {
    return this.collectCustomerForm.get('family_name') as FormControl;
  }

  get company_name(): AbstractControl {
    return this.collectCustomerForm.get('company_name') as FormControl;
  }

  get postal_code(): AbstractControl {
    return this.collectCustomerForm.get('postal_code') as FormControl;
  }

  get email(): AbstractControl {
    return this.collectCustomerForm.get('email') as FormControl;
  }

  get phone_number(): AbstractControl {
    return this.collectCustomerForm.get('phone_number') as FormControl;
  }

  get city(): AbstractControl {
    return this.collectCustomerForm.get('city') as FormControl;
  }

  get address_line1(): AbstractControl {
    return this.collectCustomerForm.get('address_line1') as FormControl;
  }

  get country_code(): AbstractControl {
    return this.collectCustomerForm.get('country_code') as FormControl;
  }

  public checkFormFieldValidity(formField: AbstractControl): boolean {
    return formField.invalid && !formField.value;
  }

  public checkPatternValidity(formField: AbstractControl): boolean {
    return formField.invalid && formField.value;
  }

  public toggleCompanyName() {
    this.hasCompanyName = !this.hasCompanyName;

    this.collectCustomerForm.get('company_name')?.updateValueAndValidity();
  }

  onSubmit() {
    this.isFormSubmitted = true;

    if (this.collectCustomerForm.valid) {
      const formData: Customer = {
        data: {
          customer: {
            email: this.email.value,
            phone_number: this.phone_number.value,
          },
          customer_billing_detail: {
            postal_code: this.postal_code.value,
            country_code: this.country_code.value,
            city: this.city.value,
            address_line1: this.address_line1.value,
          },
        },
      };

      if (!this.hasCompanyName) {
        formData.data.customer.given_name = this.given_name.value;
        formData.data.customer.family_name = this.family_name.value;
      } else {
        formData.data.customer.company_name = this.company_name.value;
      }

      this.customerService
        .collectCustomer(formData)
        .pipe(filter((data) => data !== null))
        .subscribe(
          () => {
            this.showSuccessMessage = true;
            localStorage.setItem('customerData', JSON.stringify(formData));
          },
          (error) => {
            const errors = error.error.error.errors;
            for (let i = 0; errors.length > i; i++) {
              this.handleCustomErrors(error.error.error.errors[i]);
            }
          },
        );
    }
  }

  private handleCustomErrors(error: { field: string; message: string }) {
    if (error.field) {
      this.collectCustomerForm.get([error['field']])?.setErrors({ custom: error.message });
    } else {
      this.showErrorMessage = true;
    }
  }

  public onSuccess() {
    this.router.navigate(['collect-bank-account']);
  }

  public onError() {
    this.showErrorMessage = false;
    location.reload();
  }
}
