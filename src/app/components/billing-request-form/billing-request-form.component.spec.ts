import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingRequestFormComponent } from './billing-request-form.component';

describe('BillingRequestFormComponent', () => {
  let component: BillingRequestFormComponent;
  let fixture: ComponentFixture<BillingRequestFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BillingRequestFormComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingRequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
