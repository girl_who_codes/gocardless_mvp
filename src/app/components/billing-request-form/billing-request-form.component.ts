import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomerDataService } from '../../services/customer-data/customer-data.service';
import { Router } from '@angular/router';
import { Mandate, PaymentService } from '../../services/payment/payment.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-billing-request-form',
  templateUrl: './billing-request-form.component.html',
  styleUrls: ['./billing-request-form.component.scss'],
})
export class BillingRequestFormComponent {
  public billingRequestForm: FormGroup = this.fb.group(
    {
      currency: ['', [Validators.required]],
      scheme: ['', [Validators.required]],
    },
    { updateOn: 'blur' },
  );

  public isFormSubmitted = false;
  public errorFieldRequired = 'This field is required';
  public showSuccessMessage = false;
  public showErrorMessage = false;

  constructor(
    private fb: FormBuilder,
    private customerService: CustomerDataService,
    private router: Router,
    private paymentService: PaymentService,
    private cookieService: CookieService,
  ) {}

  get scheme(): AbstractControl {
    return this.billingRequestForm.get('scheme') as FormControl;
  }

  get currency(): AbstractControl {
    return this.billingRequestForm.get('currency') as FormControl;
  }

  public checkFormFieldValidity(formField: AbstractControl): boolean {
    return formField.invalid && !formField.value;
  }

  public onSubmit() {
    this.isFormSubmitted = true;
    if (this.billingRequestForm.valid) {
      const formData: Mandate = {
        billing_requests: {
          mandate_request: {
            currency: this.currency.value,
            scheme: this.scheme.value.toLowerCase(),
          },
        },
      };

      this.paymentService.createBillingRequest(formData).subscribe(
        (response) => {
          const requestId = response.billing_requests.id;
          this.cookieService.set('billingId', requestId);
          this.showSuccessMessage = true;
        },
        () => (this.showErrorMessage = true),
      );
    }
  }

  public onSuccess() {
    this.router.navigate(['collect-customer']);
  }

  public onError() {
    location.reload();
  }
}
