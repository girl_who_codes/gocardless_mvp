import { Component, OnInit } from '@angular/core';
import { PaymentService, Subscription } from '../../services/payment/payment.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss'],
})
export class SubscriptionComponent implements OnInit {
  public amount = '100';
  public time = '12';

  public showSuccess = false;
  public showError = false;
  public mandateId = '';

  constructor(private paymentService: PaymentService, private router: Router) {}

  ngOnInit(): void {
    this.paymentService
      .getMandate()
      .pipe(map((response) => response.mandates[0].id))
      .subscribe(
        (id) => (this.mandateId = id),
        (error) => console.error(error),
      );
  }

  public onSubmit() {
    const data: Subscription = {
      subscriptions: {
        amount: '10000',
        currency: 'GBP',
        interval_unit: 'monthly',
        links: {
          mandate: this.mandateId,
        },
      },
    };

    this.paymentService.createSubscription(data).subscribe(
      () => (this.showSuccess = true),
      () => (this.showError = true),
    );
  }

  public onSuccess() {
    this.router.navigate(['']);
  }

  public onError() {
    location.reload();
  }
}
